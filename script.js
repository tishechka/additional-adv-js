class WhackAMole {
    constructor(rows, cols, difficulty) {
        this.rows = rows;
        this.cols = cols;
        this.difficulty = difficulty;
        this.score = { human: 0, computer: 0 };
        this.timer = null;
        this.initializeBoard();
    }

    initializeBoard() {
        const gameBoard = document.getElementById('gameBoard');
        gameBoard.innerHTML = '';

        for (let i = 0; i < this.rows; i++) {
            const row = document.createElement('tr');
            for (let j = 0; j < this.cols; j++) {
                const cell = document.createElement('td');
                row.appendChild(cell);
            }
            gameBoard.appendChild(row);
        }
    }

    start() {
        this.resetScore();
        this.timer = setInterval(() => this.showMole(), this.getInterval());
    }

    resetScore() {
        this.score.human = 0;
        this.score.computer = 0;
    }

    getInterval() {
        switch (this.difficulty) {
            case 'easy':
                return 1500;
            case 'medium':
                return 1000;
            case 'hard':
                return 500;
            default:
                return 1500;
        }
    }

    showMole() {
        const gameBoard = document.getElementById('gameBoard');
        const randomRow = Math.floor(Math.random() * this.rows);
        const randomCol = Math.floor(Math.random() * this.cols);
        const cell = gameBoard.rows[randomRow].cells[randomCol];

        cell.classList.add('mole');

        setTimeout(() => {
            cell.classList.remove('mole');
            if (!cell.classList.contains('hit')) {
                cell.classList.add('miss');
                this.score.computer++;
            }
        }, this.getInterval() / 2);
    }

    hitMole(row, col) {
        const cell = document.getElementById('gameBoard').rows[row].cells[col];
        if (cell.classList.contains('mole')) {
            cell.classList.remove('mole');
            cell.classList.add('hit');
            this.score.human++;
        } else {
            cell.classList.add('miss');
            this.score.human--;
        }

        if (this.isGameFinished()) {
            clearInterval(this.timer);
            this.displayResult();
        }
    }

    isGameFinished() {
        return this.score.human + this.score.computer >= (this.rows * this.cols) / 2;
    }

    displayResult() {
        alert(`Гра закінчена! Рахунок: Гравець - ${this.score.human}, Комп'ютер - ${this.score.computer}`);
    }
}

let game;

function startGame() {
    const difficulty = document.getElementById('difficulty').value;
    game = new WhackAMole(10, 10, difficulty);
    game.start();

    const cells = document.querySelectorAll('td');
    cells.forEach((cell, index) => {
        cell.addEventListener('click', () => handleCellClick(index));
    });
}

function handleCellClick(index) {
    const row = Math.floor(index / 10);
    const col = index % 10;
    game.hitMole(row, col);
}